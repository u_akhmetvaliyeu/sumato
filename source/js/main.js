var minWidth = 575;
var maxWidth = 1440;
var slickSettings = {
  centerMode: true,
  dots: true,
  infinite: false,
  arrows: false,
  slidesToShow: 1,
  slidesToScroll: 1,
  mobileFirst: true,
  variableWidth: true,
  responsive: [
    {
      breakpoint: 0,
      settings: 'unslick'
    },
    {
      breakpoint: minWidth,
      centerMode: true,
      dots: true,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1
    },
    {
      breakpoint: maxWidth - 1,
      settings: 'unslick'
    }
  ]
};

slickInit = function(){
  $('.articles-list:not(.slick-initialized)').slick(slickSettings);
};

slickInit();

$(window).on('resize', function(){
  var width = $(window).width();
  if(width < maxWidth && width > minWidth) {
    slickInit();
  }
});
